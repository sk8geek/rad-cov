; Radiator Cover Display
; Version 0.1
; February 2004
; (C) Copyright 1998-2004 Steven J Lilley
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
; GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License
; along with this program; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
;

; Define processor/radix

	list	p=16F84, r=HEX
	errorlevel -302

; Define CONSTANTS
;

; **** System Registers ****
; (Bank 0)

	constant INDF=0x00		; Indirect address reference, refers to register held in FSR
	constant STATUS=0x03		; Status Register
	constant FSR=0x04		; Indirect address pointer
	constant PORTA=0x05    		; Port A register
	constant PORTB=0x06    		; Port B register
	constant EEDATA=0x08		; EEPROM data register
	constant EEADR=0x09		; EEPROM address register
	constant INTCON=0x0B		; Interrupt control register
	
; (Bank 1)

	constant OPTREG=0x81  		; Option register
	constant TRISA=0x85    		; Port A data direction register
	constant TRISB=0x86    		; Port B data direction register
	constant EECON1=0x88		; EEPROM control register
	constant EECON2=0x89		; EEPROM control register


; ****  User Registers  ****

; general purpose

TMP		equ	0x0c
TMP2		equ	0x0d

BYTE1		equ	0x0e
BYTE2		equ	0x0f

SECS		equ	0x10
MINS		equ	0x11
TENS		equ	0x12
HRS		equ	0x13

FLAGS		equ	0x14
CONTROL		equ	0x15
ENABLES		equ	0x16

UFOstate	equ	0x17
CDNstate	equ	0x18
BLSstate	equ	0x19
ALNstate	equ	0x20
PLNstate	equ	0x21
STRstate	equ	0x22

MODE		equ	0x23

SUBflag		equ	0x0
SECflag		equ	0x1
MINflag		equ	0x2

; CONTROL reg bits
UFOctrl		equ	0x0
CDNctrl		equ	0x1
BLSctrl		equ	0x2
MODctrl		equ	0x3
ALNctrl		equ	0x4
PLNctrl		equ	0x5
STRctrl		equ	0x6
SDNctrl		equ	0x7

; ENABLE reg bits
MOOctrl		equ	0x3


; PORTB
LEDAlien	equ	0x1
LEDPlanet	equ	0x2
LEDMoon		equ	0x3

; PORTA
CDNEnable	equ	0x2
LEDRocket	equ	0x3
LEDEnable	equ	0x4
; ****  Bit references  ****

; STATUS register

REGBANK		equ 	0x5		; The bank indicator bit
ZEROFLAG	equ 	0x2		; The ZERO flag bit
CARRYBIT	equ 	0x0		; The CARRY bit

WREG		equ	0x0
SELF		equ	0x1

; INTCON register

GIE		equ	0x7		; Global Interrupt Enable bit
EEIEN		equ	0x6
T0IEN		equ	0x5
INTEN		equ	0x4
RBIEN		equ	0x3
T0IFLG		equ	0x2
INTFLG		equ	0x1
RBIFLG		equ	0x0

; PORTB register


; **** Macros ****

ENABLE_INTERRUPTS macro
	bsf	INTCON,GIE
	endm

DISABLE_INTERRUPTS macro
	bcf	INTCON,GIE
	btfsc	INTCON,GIE
	endm
	
SWITCH_TO_BANK0 macro
	bcf	STATUS,REGBANK
	endm

SWITCH_TO_BANK1 macro
	bsf	STATUS,REGBANK
	endm


; *************************************************
; **                                             **
; **  POWER ON RESET                             **
; **                                             **
; **  POR handling.  Calls Initialize routine    **
; **                                             **
; *************************************************

POR:	org 0x000
	call 	Initialize
Standby:	
	sleep
	nop
	
Int:	org 0x004
	DISABLE_INTERRUPTS
	btfsc	INTCON, INTFLG
	call	LightChange

Run:	btfsc	CONTROL, SDNctrl
	goto	Shutdown
	call	Timer
	call	Control
	call	Stars
	call	CDown
	call	BlastOff
	call	UFO
	call	Alien
	btfsc	CONTROL, MODctrl
	goto	ModeControl
	goto	Run

Command:
	btfsc	CONTROL, SDNctrl
	goto 	Shutdown
	call 	Timer
	call	Control
	call	Feedback
	btfsc	CONTROL, MODctrl
	goto	ModeControl
	goto	Command
	
Shutdown:
	clrf	MODE
	call	RegReset
	goto	Standby
	

ModeControl:
	return

Feedback:
	return
	
Timer:	clrf	FLAGS
	incf	BYTE1, SELF
	incfsz	BYTE1, SELF
	return
	incf	BYTE2, SELF
	bsf	FLAGS, SUBflag
	movf	BYTE2, WREG
	xorlw	0x07
	btfss	STATUS, ZEROFLAG
	return
Secs:	clrf	BYTE2
	incf	SECS, SELF
	bsf	FLAGS, SECflag
	movf	SECS, WREG
	xorlw	0x3c
	btfss	STATUS, ZEROFLAG
	return
Mins:	clrf	SECS
	incf	MINS, SELF
	bsf	FLAGS, MINflag
	movf	MINS, WREG
	xorlw	0x3c
	btfss	STATUS, ZEROFLAG
	return
Hrs:	clrf	MINS
	movlw	0xfe
	movwf	PORTB
	return
	

UFO:	btfss	CONTROL, UFOctrl
	return
	movf	PORTA, WREG
	andlw	0xfc
	movwf	TMP
;	movf	UFOstate, WREG
	btfss	FLAGS, SUBflag
	goto	UFO_no_inc
	incf	UFOstate, SELF
	; test for start alien
	movlw	0x10
	xorwf	UFOstate, WREG
	btfsc	STATUS, ZEROFLAG
	call	InitALN
	; test for stop UFO
	movlw	0x80
	xorwf	UFOstate, WREG
	btfsc	STATUS, ZEROFLAG
	goto	StopUFO
UFO_no_inc:
	movf	UFOstate, WREG
	andlw	0x03
	addwf	TMP, WREG
	movwf	PORTA
	return
StopUFO:
	clrf	UFOstate
	movlw	0x07
	iorwf	PORTA, SELF
	bcf	CONTROL, UFOctrl
	; also disable Alien
	call	StopALN
	return


Alien:	btfss	CONTROL, ALNctrl
	return
	btfss	FLAGS, SECflag
	return
	decfsz	ALNstate, SELF
	return
StopALN:
	bsf	PORTB, LEDAlien
	movlw	0x12
	movwf	ALNstate
	bcf	CONTROL, ALNctrl
	return


CDown:	btfss	CONTROL, CDNctrl
	return

	btfss	FLAGS, SECflag
	goto	CDownStrobe
	; the SEC flag is set test for blastoff
	; and count down
	movf	CDNstate, WREG
	xorlw	0x03
	btfsc	STATUS, ZEROFLAG
	goto	BlastOffInit
	incf	CDNstate, SELF

CDownStrobe:
	; save bits 2-7 to TMP
	movf	PORTA, WREG
	andlw	0xfc
	movwf	TMP

	; transform next value of LEDs so that three LEDs light 
	incf	PORTA, WREG
	andlw	0x03
	movwf	TMP2
	; TMP2 has actual next PORTA
	; now change to match countdown

	btfsc	CDNstate, 0x1
	goto	MatchCDNState
	
	btfss	CDNstate, 0x0
	goto	BlockOneOne
	
	; test for 11
	movlw	0x03
	xorwf	TMP2, WREG
	btfsc	STATUS, ZEROFLAG
	goto	SetLow
	
	; test for 00
	movf	TMP2, WREG
	btfss	STATUS, ZEROFLAG
	goto	Strobe
	
	; SetHigh
	movlw	0x10
	goto	Strobe

SetLow:
	movlw	0x01
	goto	Strobe

BlockOneOne:
	movlw	0x03
	xorwf	TMP2, WREG
	btfsc	STATUS, ZEROFLAG
	clrf	TMP2
	movf	TMP2, WREG
	goto	Strobe
	
MatchCDNState:
	movf	CDNstate, WREG

Strobe:	andlw	0x03
	addwf	TMP, WREG
	movwf	PORTA

	return	
BlastOffInit:
	clrf	CDNstate
	bcf	CONTROL, CDNctrl
	bsf	CONTROL, BLSctrl
	return


BlastOff:
	btfss	CONTROL, BLSctrl
	return
	btfsc	FLAGS, SECflag
	decf	BLSstate, SELF
	movf	BLSstate, WREG
	btfss	STATUS, ZEROFLAG
	goto	FireRockets
	bcf	CONTROL, BLSctrl
	movlw	0x15
	movwf	BLSstate
	bsf	PORTA, LEDRocket
	return
FireRockets:
	btfsc	BLSstate, 0x4
	goto	Ignition
	bcf	PORTA, LEDRocket
	return
Ignition:
	btfss	FLAGS, SUBflag
	return
	movlw	0x08
	xorwf	PORTA, SELF
	return


Control:
	; only process control changes when 
	; at a whole second.  When a minute rolls over
	; the second flag should also be set
	btfss	FLAGS, SECflag
	return

	; test MODE and then call appropriate routine
	btfsc	MODE, 0x1
	goto	ChangeStars

	; else run as normal
	
RunNormal:
	
	movf	MINS, WREG
	andlw	0x03
	xorlw	0x03
	btfsc	STATUS, ZEROFLAG
	goto	SkipCDN
	movf	SECS, WREG
	xorlw	0x20
	btfsc	STATUS, ZEROFLAG
	call	InitCDN

SkipCDN:
	btfss	MINS, 0x0
	goto	SkipUFO
	movf	SECS, WREG
	xorlw	0x04
	btfsc	STATUS, ZEROFLAG
	call	InitUFO

SkipUFO:
	movf	MINS, WREG
	xorlw	0x13
	btfsc	STATUS, ZEROFLAG
	call	StopPLN

	movf	MINS, WREG
	xorlw	0x12
	btfsc	STATUS, ZEROFLAG
	call	InitMOO
	
	movf	MINS, WREG
	xorlw	0x14
	btfsc	STATUS, ZEROFLAG
	bsf	PORTA, LEDEnable

	movf	MINS, WREG
	xorlw	0x18
	btfsc	STATUS, ZEROFLAG
	bsf	CONTROL, SDNctrl	
	goto	ControlReturn


ChangeStars:
	incf	STRstate, SELF
	movlw	0x03
	xorwf	STRstate, WREG
	btfsc	STATUS, ZEROFLAG
	clrf	STRstate
	btfsc	STRstate, 0x0
	call	InitPoleStar
	btfsc	STRstate, 0x1
	call	InitDarkStar
	; reset MODE to Normal
	clrf	MODE
	incf	MODE, SELF
	goto	ControlReturn


RunTest:
	movf	SECS, WREG
	xorlw	0x0a
	btfsc	STATUS, ZEROFLAG
	call	InitUFO

	movf	MINS, WREG
	xorlw	0x02
	btfsc	STATUS, ZEROFLAG
	call	InitMOO
	


ControlReturn:
	return

InitCDN:
	btfsc	ENABLES, CDNctrl
	return
	clrf	CDNstate
	decf	CDNstate, SELF
	; init PORTA
	movf	PORTA, WREG
	andlw	0xf8
	iorlw	0x04
	movwf	PORTA
	; stop UFO
	bcf	CONTROL, UFOctrl
	bsf	CONTROL, CDNctrl
	return

InitUFO:
	btfsc	ENABLES, UFOctrl
	return
	clrf	UFOstate
	; init PORTA
	movf	PORTA, WREG
	andlw	0xf8
	movwf	PORTA
	; stop CDN
	bcf	CONTROL, CDNctrl
	bsf	CONTROL, UFOctrl
	return


InitALN:
	movlw	0x0a
	movwf	ALNstate
	bsf	CONTROL, ALNctrl
	bcf	PORTB, LEDAlien
	return

InitPLN:
	btfsc	ENABLES, PLNctrl
	return
	bcf	PORTB, LEDPlanet
	bsf	CONTROL, PLNctrl
	return

StopPLN:
	bsf	PORTB, LEDPlanet
	bcf	CONTROL, PLNctrl
	return



InitMOO:
	btfsc	ENABLES, MOOctrl
	return
	bsf	ENABLES, UFOctrl
	bsf	ENABLES, CDNctrl
	bcf	PORTB, LEDMoon
	return

StopMOO:
	bsf	PORTB, LEDMoon
	return


LightChange:
	; test for wake-up
	movf	MODE, SELF
	btfsc	STATUS, ZEROFLAG
	goto	RunMode
	; otherwise increment mode
	rlf	MODE, SELF
	goto	LightChangeReturn
RunMode:
	bcf	PORTA, LEDEnable
	call	InitPLN
	bsf	CONTROL, STRctrl
	movlw	0x1
	movwf	MODE
LightChangeReturn:
	bcf	INTCON, INTFLG
	retfie


	
Stars:	

	btfss	CONTROL, STRctrl
	return
	movf	STRstate, SELF
	btfss	STATUS, ZEROFLAG
	return
	movf	PORTB, WREG
	andlw	0x0f
	movwf	TMP
	movf	PORTB, WREG
	andlw	0xf0
	addlw	0x10
	addwf	TMP, WREG
	movwf	PORTB
	return

InitPoleStar:
	; set STRstate
	movlw	0x01
	movwf	STRstate
	; set PORTB
	movf	PORTB, WREG
	andlw	0x0f
	movwf	PORTB
	return
	
InitDarkStar:
	; set STRstate
	movlw	0x02
	movwf	STRstate
	; set PORTB
	movf	PORTB, WREG
	andlw	0x0f
	iorlw	0xf0
	movwf	PORTB
	return



; ****************
; ** Initialize **
; ****************

Initialize:
	DISABLE_INTERRUPTS 
	goto  	Initialize		; (make sure)
	
; set MCU registers
; set INT control for INT and PORTB
	SWITCH_TO_BANK1
	movlw	0x70
	movwf	OPTREG
	movlw 	0x00
	movwf 	TRISA			; set PORTA directions
	movlw	0x01
	movwf 	TRISB			; set PORTB directions
	SWITCH_TO_BANK0
	movlw 	0x10
	movwf 	INTCON			; set INTCON from W
	call	RegReset
	clrf	MODE
	retfie				; return and set global interrupt enable
	
RegReset:
	movlw	0xfe
	iorwf	PORTB, SELF
	movlw	0x1f
	movwf	PORTA
	clrf	BYTE1
	clrf	BYTE2
	clrf	SECS
	clrf	MINS
	clrf	TENS
	clrf	HRS
	clrf	CONTROL
	clrf	FLAGS
	clrf	ENABLES
	clrf	UFOstate
	clrf	CDNstate
	clrf	STRstate
	movlw	0x12
	movwf	BLSstate
	movwf	ALNstate
	return	

	end
	
	
